from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *
from django.urls import reverse_lazy, reverse

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q

from urllib.parse import urlparse, parse_qs
from django.conf import settings

from django.http import JsonResponse



# Client BaseMixin View
class ClientMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["root_categories"] = ProductCategory.objects.filter(root=None)
		context["allproducts"] = Product.objects.all()
		context["cform"] = CustomerSignupForm

		# Assigning customer to recently created or already present cart id
		cart_id = self.request.session.get("mycart", None)
		logged_in_user = self.request.user
		if logged_in_user.is_authenticated and logged_in_user.groups.filter(name = "customer").exists():
			customer = Customer.objects.get(user=logged_in_user)
			if cart_id:
				cart_obj = Cart.objects.get(id=cart_id)
				cart_obj.customer = customer
				cart_obj.save()

		return context



# Client CustomerRequiredMixin View
class CustomerRequiredMixin(object):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and request.user.groups.filter(name="customer").exists():
			pass
		else:
			return redirect("ecomapp:customerlogin")

		return super().dispatch(request, *args, **kwargs)




#Client Home View
class ClientHomeView(ClientMixin, TemplateView):
	template_name = "clienttemplates/clienthome.html"



# Client Category Detail View
class CategoryDetailView(ClientMixin, DetailView):
	template_name = "clienttemplates/clientcategorydetail.html"
	model = ProductCategory
	context_object_name = "categoryobject"



# Client AddToCart View
class AddToCartView(TemplateView):
	template_name = 'clienttemplates/clientaddtocart.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		product_id = self.kwargs["pk"]
		product = Product.objects.get(id = product_id)
		cart_id = self.request.session.get("mycart", None)
		if cart_id:
			mycart = Cart.objects.get(id=cart_id)
			print("old cart")
		else:
			mycart = Cart.objects.create(total=0)
			self.request.session["mycart"] = mycart.id
			print("new cart")

		cartitemquery = mycart.cartproduct_set.filter(product = product) 
		if cartitemquery.exists():
			cartprod = cartitemquery.first()
			cartprod.quantity += 1
			cartprod.subtotal += product.sp
			cartprod.save()
			mycart.total += product.sp
			mycart.save()
			print("Item already exists in cart***")
		else:
			cartprod = CartProduct.objects.create(
				cart=mycart, 
				product=product, 
				rate=product.sp, 
				quantity=1, 
				subtotal=product.sp)
			mycart.total += product.sp
			mycart.save()

			print("Item deosnt exists in cart***")

		return context



class CartView(ClientMixin, TemplateView):
	template_name = "clienttemplates/cart.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		cart_id = self.request.session.get("mycart", None)
		try:
			cart = Cart.objects.get(id = cart_id)
		except:
			cart = None
		context["mycart"] = cart

		return context


# Client Cart Manage View
class ManageCartView(ClientMixin, TemplateView):
	template_name = 'clienttemplates/clientmanagecart.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		cartproduct_id = self.kwargs["pk"]
		action = self.kwargs["action"]
		cartproduct_obj = CartProduct.objects.get(id = cartproduct_id)
		cart = cartproduct_obj.cart
		#cart_id = self.request.session.get("mycart", None)
		#cart = Cart.objects.get(id = cart_id)
		if action == 'inr':
			cartproduct_obj.quantity += 1
			cartproduct_obj.subtotal += cartproduct_obj.rate
			cartproduct_obj.save()
			cart.total += cartproduct_obj.rate
			cart.save()
			context["message"] = "product '" + cartproduct_obj.product.title + "' increased successfully"
		elif action == 'dcr':
			cartproduct_obj.quantity -= 1
			cartproduct_obj.subtotal -= cartproduct_obj.rate
			cartproduct_obj.save()
			cart.total -= cartproduct_obj.rate
			cart.save()
			if cartproduct_obj.quantity == 0:
				cartproduct_obj.delete()
			context["message"] = "product '" + cartproduct_obj.product.title + "' decreased successfully"
		elif action == 'rmv':
			product_title = cartproduct_obj.product.title
			cart.total -= cartproduct_obj.subtotal
			cart.save()
			cartproduct_obj.delete()
			context["message"] = product_title + " removed from cart successfully"
		else:
			context["message"] = "Invalid operation"

		return context


class EmptyCartView(TemplateView):
	template_name = 'clienttemplates/clientmanagecart.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		c_id = self.request.session.get("mycart", None)
		if c_id:
			cart_obj = Cart.objects.get(id = c_id)
			cartproducts = cart_obj.cartproduct_set.all()
			cart_obj.total = 0
			cart_obj.save()
			cartproducts.delete()
			context["message"] = 'cart is now empty'
		else:
			context["message"] = "Order first to empty cart..."


		return context



class OrderCreateView(ClientMixin, CustomerRequiredMixin, CreateView):
	template_name = 'clienttemplates/ordercreate.html'
	form_class = OrderForm
	success_url = reverse_lazy("ecomapp:clienthome")

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		cart_id = self.request.session.get("mycart", None)
		cart = Cart.objects.get(id = cart_id)
		context["cart_obj"] = cart

		return context

	def form_valid(self, form):
		cart = Cart.objects.get(id = self.request.session.get("mycart"))
		form.instance.cart = cart
		form.instance.customer = cart.customer
		form.instance.subtotal = cart.total
		form.instance.discount = 0
		form.instance.total = cart.total
		form.instance.order_status = "orderplaced"
		del self.request.session["mycart"]
		# cart_id = self.request.session.get("mycart")
		# del cart_id


		return super().form_valid(form)




# Client Customer Signup View
class CustomerSignupView(ClientMixin, CreateView):
	template_name = 'clienttemplates/customersignup.html'
	form_class = CustomerSignupForm
	success_url = reverse_lazy("ecomapp:clienthome")

	def form_valid(self, form):
		uname = form.cleaned_data["username"]
		email = form.cleaned_data["email"]
		password = form.cleaned_data["password"]
		user = User.objects.create_user(uname, email, password)
		form.instance.user = user

		return super().form_valid(form)


# Customer Login View Class
class CustomerLoginView(FormView):
	template_name = 'clienttemplates/customerlogin.html'
	form_class = SigninForm
	success_url = reverse_lazy("ecomapp:clienthome")
	
	def form_valid(self, form):
		uname = form.cleaned_data['username']
		pwrd = form.cleaned_data['password']
		user = authenticate(username=uname, password=pwrd)
		if user is not None:
			login(self.request, user)
		else:
			return render(self.request, self.template_name,{
				'error': 'Invalid Username or Password!!!',
				'form': form
				})
		return super().form_valid(form)

	def get_success_url(self):
		logged_in_user = self.request.user
		if logged_in_user.groups.filter(name = 'customer').exists():
			return reverse("ecomapp:clienthome")
		else:
			return reverse("ecomapp:customersignup")




# Client Search View class
class ClientSearchView(TemplateView):
	template_name = "clienttemplates/clientsearch.html"

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['allproducts'] = Product.objects.all()
		context['allcats'] = ProductCategory.objects.all()
		keyword = self.request.GET["query"]
		#keyword = self.request.GET.get("dipak")
		# items = News.objects.filter(title__icontains = keyword)
		products = Product.objects.filter(
			Q(title__icontains = keyword) | 
			Q(category__title__icontains = keyword) | 
			Q(sku__icontains = keyword))

		context["keyword"] = keyword

		page = self.request.GET.get('page', 1)

		paginator = Paginator(products, 8)
		try:
		    results = paginator.page(page)
		except PageNotAnInteger:
		    results = paginator.page(1)
		except EmptyPage:
		    results = paginator.page(paginator.num_pages)
		#for pagination

		context["allproducts"] = results



		print(products, "***********************")
		
		return context








# Admin Signin View Class
class AdminSigninView(FormView):
	template_name = 'admintemplates/adminsignin.html'
	form_class = SigninForm
	success_url = reverse_lazy("ecomapp:adminhome")

	def form_valid(self, form):
		a = form.cleaned_data['username']
		b = form.cleaned_data['password']
		user = authenticate(username=a, password=b)
		if user is not None:
			login(self.request, user)
		else:
			return render(self.request, "admintemplates/adminsignin.html",{
				'error': 'Invalid Username or Password!!!',
				'form': form
				})
		return super().form_valid(form)


# Admin  Signout view class
class AdminSignoutView(View):
	def get(self, request):
		logout(request)
		return redirect("/")


# Admin Search View class
class SearchView(TemplateView):
	template_name = "admintemplates/search.html"

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['allproducts'] = Product.objects.all()
		context['allcarts'] = Cart.objects.all()
		keyword = self.request.GET["query"]
		#keyword = self.request.GET.get("dipak")
		# items = News.objects.filter(title__icontains = keyword)
		products = Product.objects.filter(
			Q(title__icontains = keyword) | 
			Q(sku__icontains = keyword) | 
			Q(category__slug__icontains = keyword))
		context["keyword"] = keyword

		page = self.request.GET.get('page', 1)

		paginator = Paginator(products, 8)
		try:
		    results = paginator.page(page)
		except PageNotAnInteger:
		    results = paginator.page(1)
		except EmptyPage:
		    results = paginator.page(paginator.num_pages)
		#for pagination

		context["allproducts"] = results



		print(products, "***********************")
		
		return context


class AjaxSearchView(View):

    def get(self, request):
        keyword = request.GET.get('query')
        results = Product.objects.filter(Q(title__startswith=keyword) | 
        Q(sku__startswith=keyword) | 
        Q(category__slug__startswith=keyword))
        print(results, "*******************")
        data = []
        for result in results:
            data.append(result.title)
            print(results, "&&&&&&&&&&&&&&&")
        print(data)
        return JsonResponse({'results': data})
#***********************
#***********************
#***********************
#***********************
#***********************


#Admin Views starts

# AdminRequiredMixin Class
class AdminRequiredMixin(object):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated:
			pass
		else:
			return redirect("/ecom-admin/signin/")

		return super().dispatch(request, *args, **kwargs)



# Admin Mixin Class
class AdminMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)

		return context


# Admin Home View 
class AdminHomeView(AdminRequiredMixin, AdminMixin, TemplateView):
	template_name = 'admintemplates/adminhome.html'



# Admin Products View
class AdminProductListView(AdminRequiredMixin, AdminMixin, ListView):
	template_name = 'admintemplates/adminproductlist.html'
	queryset = Product.objects.all().order_by("-id")
	context_object_name = "allproducts"
	paginate_by = 8



# Admin Products Detail View
class AdminProductDetailView(AdminRequiredMixin, AdminMixin, DetailView):
	template_name = 'admintemplates/adminproductdetail.html'
	model = Product
	context_object_name = 'productobject'



# Admin Cart List View
class AdminCartListView(AdminRequiredMixin, AdminMixin, ListView):
	template_name = 'admintemplates/admincartlist.html'
	queryset = Cart.objects.all().order_by("-id")
	context_object_name = 'allcarts'



# Admin Cart Detail View
class AdminCartDetailView(AdminRequiredMixin, AdminMixin, DetailView):
	template_name = 'admintemplates/admincartdetail.html'
	model = Cart
	context_object_name = "cartobject"


# Admin Order List View
class AdminOrderList(AdminRequiredMixin, AdminMixin, ListView):
	template_name = 'admintemplates/adminorderlist.html'
	queryset = Order.objects.all().order_by("-id")
	context_object_name = 'allorders'


# Admin Order Detail View
class AdminOrderDetailView(AdminRequiredMixin, AdminMixin, DetailView):
	template_name = 'admintemplates/adminorderdetail.html'
	model = Order
	context_object_name = "orderobject"


