from django import forms
from .models import *
from django.contrib.auth.models import User




# Signin Form class
class SigninForm(forms.Form):
	username = forms.CharField(widget = forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter username...'
		}))
	password = forms.CharField(widget = forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter password...'
		}))


# Signup Form Class
class CustomerSignupForm(forms.ModelForm):
	username = forms.CharField(widget= forms.TextInput)
	email = forms.EmailField(widget= forms.EmailInput)
	password = forms.CharField(widget= forms.PasswordInput)
	confirm_password = forms.CharField(widget = forms.PasswordInput)
	class Meta:
		model = Customer
		fields = ['username', 'email', 'password', 'confirm_password', 'name', 'mobile', 'address', 'photo']



class OrderForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = ["deliver_date", 'payment_method', 'municipality', 'street_address', 'alt_mobile']