from django.contrib import admin
from .models import *


admin.site.register([Admin, Customer, Organization, ProductCategory,
				 Product, ProductImage, Cart, CartProduct, Order])