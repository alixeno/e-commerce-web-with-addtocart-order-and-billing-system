from django.db import models
from django.contrib.auth.models import User, Group
from .constants import *


class TimeStamp(models.Model):
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True, null = True, blank = True)
	active = models.BooleanField(default = True)

	class Meta: 
		abstract = True


class Admin(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	mobile = models.CharField(max_length = 300)
	name = models.CharField(max_length = 300)
	address = models.CharField(max_length = 300)
	photo = models.ImageField(upload_to = 'user')

	def __str__(self):
		return self.name



class Customer(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	mobile = models.CharField(max_length = 300)
	name = models.CharField(max_length = 300)
	address = models.CharField(max_length = 300, null = True, blank = True)
	photo = models.ImageField(upload_to = 'user')

	def save(self, *args, **kwargs):
		grp, created = Group.objects.get_or_create(name ="customer")
		self.user.groups.add(grp)
		super().save(*args, **kwargs)


	def __str__(self):
		return self.name



class Organization(TimeStamp):
	name = models.CharField(max_length = 300)
	logo = models.ImageField(upload_to = "restro")
	email = models.EmailField()
	phone = models.CharField(max_length = 50)
	mobile = models.CharField(max_length = 50, null = True, blank = True)
	address = models.CharField(max_length = 200)
	about = models.TextField()
	profile_image = models.ImageField(upload_to = "restro")
	website = models.CharField(max_length = 100)
	map_location = models.CharField(max_length = 500)
	favicon = models.ImageField(upload_to = "restro")
	facebook = models.CharField(max_length = 300, null = True)
	instagram = models.CharField(max_length = 300, null = True)
	estb_date = models.DateField()

	def __str__(self):
		return self.name



class ProductCategory(TimeStamp):
	title = models.CharField(max_length = 300)
	slug = models.SlugField(unique = True)
	image = models.ImageField(upload_to = "category")
	root = models.ForeignKey('self', on_delete = models.SET_NULL, null = True, blank = True)

	def __str__(self):
		return self.title


class Product(TimeStamp):
	title = models.CharField(max_length = 300)
	# sku = stock keeping unit unique code to identify product
	sku = models.CharField(max_length= 300, unique = True)
	category = models.ForeignKey(ProductCategory, on_delete = models.CASCADE)
	mrp = models.DecimalField(max_digits=30, decimal_places=2)
	sp = models.DecimalField(max_digits=30, decimal_places=2)

	def image(self):
		if self.productimage_set.all():
			return self.productimage_set.first().image
		else:
			return None

	def __str__(self):
		return self.title



class ProductImage(TimeStamp):
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	image = models.ImageField(upload_to='product')



class Cart(TimeStamp):
	customer = models.ForeignKey(Customer, on_delete= models.CASCADE, null=True, blank=True)
	total = models.DecimalField(max_digits=30, decimal_places=2)

	def __str__(self):
		return "Cart id: " + str(self.id)


class CartProduct(TimeStamp):
	cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	rate = models.DecimalField(max_digits=20, decimal_places=2)
	quantity = models.PositiveIntegerField()
	subtotal = models.DecimalField(max_digits=20, decimal_places=2)

	def __str__(self):
		return self.product.title + "(" + str(self.cart.id) + ")"



class Order(TimeStamp):
	cart = models.OneToOneField(Cart, on_delete=models.CASCADE)
	customer = models.OneToOneField(Customer, on_delete=models.CASCADE, null=True, blank=True)
	subtotal = models.DecimalField(max_digits=20, decimal_places=2)
	discount = models.DecimalField(max_digits=20, decimal_places=2)
	total = models.DecimalField(max_digits=20, decimal_places=2)
	deliver_date = models.DateField(null=True, blank=True)
	municipality = models.CharField(max_length=200, null=True, blank=True)
	street_address = models.CharField(max_length=200, null=True, blank=True)
	alt_mobile = models.CharField(max_length=50, null=True, blank=True)
	order_status = models.CharField(max_length=50, choices=ORDER_STATUS)
	payment_method = models.CharField(max_length=50, choices = PAYMENT_METHOD)


	def __str__(self):
		return "Order id: " + str(self.id)
