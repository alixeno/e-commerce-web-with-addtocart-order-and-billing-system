from django.urls import path
from .views import *

app_name = "ecomapp"

urlpatterns = [


# Client Home View
	path("", ClientHomeView.as_view(), name = 'clienthome'),

	path("category/<slug:slug>/", 
		CategoryDetailView.as_view(), name = 'categorydetail'),


	path("add-to-cart/<int:pk>/",
		AddToCartView.as_view(), name = "addtocart"),

	path("cart/", CartView.as_view(), name = 'cart'),

	path("manage-cart/<int:pk>/<action>/", 
		ManageCartView.as_view(), name = 'managecart'),

	path("empty-cart/", 
		EmptyCartView.as_view(), name = 'emptycart'),

	path("customer/sign-up/", CustomerSignupView.as_view(), name = 'customersignup'),
	path("customer/login/", CustomerLoginView.as_view(), name = 'customerlogin'),


	# Signin Url Start
	path("ecom-admin/signin/", AdminSigninView.as_view(), name = 'adminsignin'),
	path("ecom-admin/signout/", AdminSignoutView.as_view(), name = 'adminsignout'),


    path("search/", SearchView.as_view(), name = "search"),
    path("customer-search/", ClientSearchView.as_view(), name = "clientsearch"),

    path("ajaxsearch/", AjaxSearchView.as_view(), name = "ajaxsearch"),

    path("order/", OrderCreateView.as_view(), name = "ordercreate"),




#*************************
#*************************
#*************************
#*************************
#*************************


	# Admin Url Starts

	#Admin home view
	path("ecom-admin/", AdminHomeView.as_view(), name = 'adminhome'),

	path("ecom-admin/product/list/", AdminProductListView.as_view(), name = 'adminproductlist'),

	path("ecom-admin/cart/list/", AdminCartListView.as_view(), name = 'admincartlist'),

	path("ecom-admin/cart/<int:pk>/detail/", 
		AdminCartDetailView.as_view(), name = 'admincartdetail'),

	path("ecom-admin/product/<int:pk>/detail/", 
		AdminProductDetailView.as_view(), name = 'adminproductdetail'),

	path("ecom-admin/order/list/", AdminOrderList.as_view(), name = 'adminorderlist'),

	path("ecom-admin/order/<int:pk>/detail", AdminOrderDetailView.as_view(), name = "adminorderdetail"),

	


	
]